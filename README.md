# rd-lib-common

'rapiddweller Common' is an open source Java library forked from Databene Commons by Volker Bergmann.

It provides extensions to the Java core library by utility classes, abstract concepts
and concrete implementations.

## Introduction

This library is mandatory for [rapiddweller 'Benerator'](https://www.benerator.de).

## Prerequisites

- Java 11 JDK (we recommend [adoptopenjdk](https://adoptopenjdk.net/))
- [Maven](https://maven.apache.org/)

## Docs

- Create your docs using the maven site plugin `mvn site:site`. 
- Checkout the rapiddweller benerator projects website [www.benerator.de](https://www.benerator.de/) 
  for additional support resources.  
- Checkout the maintainers website [www.rapiddweller.com](https://www.rapiddweller.com/) 
  for additional support resources.  
  

## Getting Involved

If you would like to reach out to the maintainers, contact us via our 
[Contact-Form](https://www.benerator.de/contact-us) or email us at 
[solution.benerator@rapiddweller.com](mailto:solution.benerator@rapiddweller.com).


## Contributing

Please see our [Contributing](CONTRIBUTING.md) guidelines. 

Check out the maintainers [website!](https://rapiddweller.com)
